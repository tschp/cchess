CC=gcc
BIN=bin/
TARGET=bin/cchess
HEADERS=chess.h

default: $(TARGET)

$(TARGET): $(BIN)main.o $(BIN)chess.o
	$(CC) $(BIN)main.o $(BIN)chess.o -o $(TARGET)
$(BIN)main.o: main.c
	$(CC) main.c -c -o $@
$(BIN)chess.o: chess.c chess.h
		$(CC) chess.c -c -o $@

