#define MAX_X 8
#define MAX_Y 8
#define MIN_X 0
#define MIN_Y 0


typedef enum POINT_STATES {

	PSTATE_IN_RANGE_TAKE =		1 >> 0,
	PSTATE_NORMAL =				1 >> 1,
	PSTATE_CHECK =				1 >> 2,
	PSTATE_CHECKMATE =			1 >> 3

} POINT_STATE;

typedef enum GAME_STATES {

	GSTATE_P1_MOVE =			1 >> 0,
	GSTATE_P2_MOVE =			1 >> 1,
	GSTATE_P1_WIN =				1 >> 2,
	GSTATE_P2_WIN =				1 >> 3	

} GAME_STATE;

typedef enum POINT_SIDES {

	PSIDE_P1, 
	PSIDE_P2, 
	PSIDE_NONE

} POINT_SIDE;

typedef enum POINT_TYPES {

	PTYPE_OOB,
	PTYPE_NONE,
	PTYPE_KING,
	PTYPE_QUEEN,
	PTYPE_BISHOP,
	PTYPE_KNIGHT,
	PTYPE_ROOK,
	PTYPE_PAWN,
	PTYPE_DANGER

} POINT_TYPE;

typedef enum MOVE_TYPES {
	
	MTYPE_NOTSET,
	MTYPE_NORMAL,
	MTYPE_INVALID,
	MTYPE_TAKE,
	MTYPE_CHECK,
	MTYPE_ENPASSANT,
	MTYPE_DEFAULT = MTYPE_INVALID

} MOVE_TYPE;

typedef struct {

	int x, y, prev_x, prev_y;
	POINT_TYPE ptype;
	POINT_STATE state;
	POINT_SIDE pside;
	int has_moved;
	MOVE_TYPE valid_moves[8][8];

} Point;

typedef struct {

	Point points[8][8];
	Point* last_moved;
	
} Grid;


void start_chess();
void place_point(Grid* grd, POINT_TYPE ptype, POINT_SIDE side, int pos_x, int pos_y);
Point get_point_copy(Grid* grd, int x, int y);
Point* get_point_memloc(Grid* grd, int x, int y);
void setup_grid(Grid* grd);
void modify_point();
void delete_point();
void check_valid_move();
void update_grid_context(Grid* grd);
int move_point(Grid* grd, Point* pce, int new_x, int new_y);
void check_game_state();
void print_grid(Grid* grd, Point* point);
char get_ptype_char(POINT_TYPE ptype);
void update_context_at_point(Grid* grd, int x, int y);
void move_cursor();
