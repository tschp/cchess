#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "chess.h"

Point get_point_copy(Grid* grd, int x, int y){

	// For checking out of bounds
	Point OOB = {.ptype=PTYPE_OOB};

	if (!(x >= MAX_X) && !(x < MIN_X) &&
		!(y >= MAX_X) && !(y < MIN_Y)
	)
		return grd->points[x][y];
	return OOB;
}

Point* get_point_memloc(Grid* grd, int x, int y){
	return &grd->points[x][y];
}

void place_point(Grid* grd, POINT_TYPE ptype, POINT_SIDE side, int pos_x, int pos_y){

	Point pnt = {.x=pos_x, .y=pos_y, .ptype=ptype, .state=PSTATE_NORMAL, .pside=side, .has_moved=0};
	grd->points[pos_x][pos_y] = pnt;
	
};

void setup_grid(Grid* Grid){
	
	// Initialize
	for (int y = MIN_Y; y < MAX_Y; y++) {
		for (int x = MIN_X; x < MAX_Y; x++){
			place_point(Grid, PTYPE_NONE, PSIDE_NONE, x, y);
		}
	}

	// PAWNS
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 0 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 1 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 2 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 3 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 4 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 5 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 6 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P2, 7 , 1);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 0 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 1 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 2 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 3 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 4 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 5 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 6 , 6);
	place_point(Grid, PTYPE_PAWN, PSIDE_P1, 7 , 6);

	// KINGS
	place_point(Grid, PTYPE_KING, PSIDE_P2, 4, 0);
	place_point(Grid, PTYPE_KING, PSIDE_P1, 3, 7);

	// QUEENS
	place_point(Grid, PTYPE_QUEEN, PSIDE_P2, 3, 0);
	place_point(Grid, PTYPE_QUEEN, PSIDE_P1, 4, 7);

	// ROOKS
	place_point(Grid, PTYPE_ROOK, PSIDE_P2, 0, 0);
	place_point(Grid, PTYPE_ROOK, PSIDE_P2, 7, 0);
	place_point(Grid, PTYPE_ROOK, PSIDE_P1, 0, 7);
	place_point(Grid, PTYPE_ROOK, PSIDE_P1, 7, 7);

	// KNIGHT
	place_point(Grid, PTYPE_KNIGHT, PSIDE_P2, 1, 0);
	place_point(Grid, PTYPE_KNIGHT, PSIDE_P2, 6, 0);
	place_point(Grid, PTYPE_KNIGHT, PSIDE_P1, 1, 7);
	place_point(Grid, PTYPE_KNIGHT, PSIDE_P1, 6, 7);

	// BISHOPS
	place_point(Grid, PTYPE_BISHOP, PSIDE_P2, 2, 0);
	place_point(Grid, PTYPE_BISHOP, PSIDE_P2, 5, 0);
	place_point(Grid, PTYPE_BISHOP, PSIDE_P1, 2, 7);
	place_point(Grid, PTYPE_BISHOP, PSIDE_P1, 5, 7);

	update_grid_context(Grid);
}

int move_point(Grid* grd, Point* pnt, int new_x, int new_y){

		if ((pnt->valid_moves[new_x][new_y] != MTYPE_INVALID)){

			// Copy Point value
			Point new_pnt = *pnt;
			new_pnt.prev_x = pnt->x;
			new_pnt.prev_y = pnt->y;
			new_pnt.x = new_x;
			new_pnt.y = new_y;
			new_pnt.has_moved = 1;

			// Copy new point to new location
			grd->points[new_x][new_y] = new_pnt;
			grd->last_moved = &grd->points[new_x][new_y];

			// "Remove" point at old location
			place_point(grd, PTYPE_NONE, PSIDE_NONE, pnt->x, pnt->y);

			// Unique case for enpassant
			if (new_pnt.valid_moves[new_x][new_y] == MTYPE_ENPASSANT){
				if (new_pnt.x - new_pnt.prev_x == 1)
					place_point(grd, PTYPE_NONE, PSIDE_NONE, new_pnt.x - 1, new_pnt.y);
				if (new_pnt.x - new_pnt.prev_x == -1)
					place_point(grd, PTYPE_NONE, PSIDE_NONE, new_pnt.x + 1, new_pnt.y);
			}

			return 1;
	}
	return 0;
}

void check_game_state();

void update_grid_context(Grid* grd){

	for (int y = MIN_Y; y < MAX_Y; y++){
		for (int x = MIN_X; x < MAX_X; x++){
			update_context_at_point(grd, x, y);
		}
	}

}

void update_context_at_point(Grid* grd, int ploc_x, int ploc_y) {

	Point pnt = grd->points[ploc_x][ploc_y];

	for (int y = MIN_Y; y < MAX_Y; y++) {
		for (int x = MIN_X; x < MAX_X; x++) {

			// Reset Valid Moves to default (should be invalid)
			pnt.valid_moves[x][y] = MTYPE_DEFAULT;
		}
	}
	
	// Flip y axis depending on Grid side
	int flip;
	switch (pnt.pside){
		case PSIDE_P2:
			flip = 1;
			break;
		case PSIDE_P1:
			flip = -1;
			break;
	}

	// Ints to use for positional context changes
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;

	switch(pnt.ptype){

		case PTYPE_PAWN:

			// Base Move

			y1 = flip;

			if (get_point_copy(grd, pnt.x, pnt.y + y1).ptype != PTYPE_OOB &&
				grd->points[pnt.x][pnt.y + y1].ptype == PTYPE_NONE)
				pnt.valid_moves[pnt.x][pnt.y + y1] = MTYPE_NORMAL;
			
			// Two space move

			y1 = 2 * flip;

			if (!pnt.has_moved && 
				get_point_copy(grd, pnt.x, pnt.y + y1).ptype != PTYPE_OOB &&
				(grd->points[pnt.x][pnt.y + y1].ptype == PTYPE_NONE) &&
				(grd->points[pnt.x][(pnt.y + y1) - (1*flip)].ptype == PTYPE_NONE)
			)
				pnt.valid_moves[pnt.x][pnt.y + y1] = MTYPE_NORMAL;

			// Take Piece

			y1 = 1 * flip;
			x1 = 1;
			x2 = -1;

			// Take Right
			if (get_point_copy(grd, pnt.x + x1, pnt.y + y1).ptype != PTYPE_OOB &&
				get_point_copy(grd, pnt.x + x1, pnt.y + y1).ptype != PTYPE_NONE &&
				get_point_copy(grd, pnt.x + x1, pnt.y + y1).pside != pnt.pside
			)
				pnt.valid_moves[pnt.x + x1][pnt.y + y1] = MTYPE_TAKE;

			// Take Left
			if (get_point_copy(grd, pnt.x + x2, pnt.y + y1).ptype != PTYPE_OOB &&
				get_point_copy(grd, pnt.x + x2, pnt.y + y1).ptype != (PTYPE_NONE) &&
				get_point_copy(grd, pnt.x + x2, pnt.y + y1).pside != pnt.pside
			)
				pnt.valid_moves[pnt.x + x2][pnt.y + y1] = MTYPE_TAKE;

			
			// Enpessant
			if (get_point_copy(grd, pnt.x, pnt.y + y1).ptype != PTYPE_OOB &&
				get_point_copy(grd, pnt.x, pnt.y + y1).ptype == PTYPE_PAWN &&
				get_point_copy(grd, pnt.x, pnt.y + y1).pside != pnt.pside &&
				get_point_copy(grd, pnt.x, pnt.y + y1).has_moved && // Miiight not need this, but may help drop needless calculations.
				get_point_memloc(grd, pnt.x, pnt.y + y1) == grd->last_moved &&
				abs(get_point_copy(grd, pnt.x, pnt.y + y1).prev_y - grd->points[pnt.x][pnt.y + y1].y) == 2
			) {
				pnt.valid_moves[pnt.x + x1][pnt.y + y1] = MTYPE_ENPASSANT;
				pnt.valid_moves[pnt.x - x1][pnt.y + y1] = MTYPE_ENPASSANT;

				// The only time we don't have a pnt replace the taken one directly.. so:
				//place_point(grd, PTYPE_NONE, PSIDE_NONE,grd->points[pnt->x]->x, grd->points[pnt->x]->y);
			}
			break;
		
		case PTYPE_ROOK:
			
			// Right
			for (x1 = pnt.x + 1; x1 < MAX_X; x1++){
				Point p_at_loc = get_point_copy(grd, x1, pnt.y);

				if (p_at_loc.ptype == PTYPE_NONE){
					pnt.valid_moves[x1][pnt.y] = MTYPE_NORMAL;
					continue;

				} else if ((p_at_loc.ptype != PTYPE_NONE) && (p_at_loc.pside != pnt.pside)){
					pnt.valid_moves[x1][pnt.y] = MTYPE_TAKE;
					break;
				}

			}

			// Left
			for (x1 = pnt.x - 1; x1 >= MIN_X; x1--){
				Point p_at_loc = get_point_copy(grd, x1, pnt.y);

				if (p_at_loc.ptype == PTYPE_NONE){
					pnt.valid_moves[x1][pnt.y] = MTYPE_NORMAL;
					continue;

				} else if ((p_at_loc.ptype != PTYPE_NONE) && (p_at_loc.pside != pnt.pside)){
					pnt.valid_moves[x1][pnt.y] = MTYPE_TAKE;
					break;
				}

			}

			// Down
			for (y1 = pnt.y + 1; y1 < MAX_Y; y1++){
				Point p_at_loc = get_point_copy(grd, pnt.x, y1);

				if (p_at_loc.ptype == PTYPE_NONE){
					pnt.valid_moves[pnt.x][y1] = MTYPE_NORMAL;
					continue;

				} else if ((p_at_loc.ptype != PTYPE_NONE) && (p_at_loc.pside != pnt.pside)){
					pnt.valid_moves[pnt.x][y1] = MTYPE_TAKE;
					break;
				}

				else if (p_at_loc.pside == pnt.pside){
					break;
				}
			}

			// Up

			for (y1 = pnt.y - 1; y1 > MIN_Y; y1--){
				Point p_at_loc = get_point_copy(grd, pnt.x, y1);

				if (p_at_loc.ptype == PTYPE_NONE){
					pnt.valid_moves[pnt.x][y1] = MTYPE_NORMAL;
					continue;

				} else if ((p_at_loc.ptype != PTYPE_NONE) && (p_at_loc.pside != pnt.pside)){
					pnt.valid_moves[pnt.x][y1] = MTYPE_TAKE;
					break;
				}

			}
			
			break;

		case PTYPE_BISHOP:
		
			// Top-Right
			for (x1 = pnt.x+1, y1 = pnt.y-1; x1 < MAX_X && y1 > MIN_Y; x1++, y1--) {
				if (get_point_copy(grd, x1, y1).ptype == PTYPE_NONE) {
					pnt.valid_moves[x1][y1] = MTYPE_NORMAL;

				} else if (get_point_copy(grd, x1, y1).pside != pnt.pside) {
					pnt.valid_moves[x1][y1] = MTYPE_TAKE;
					break;

				} else if (get_point_copy(grd, x1, y1).pside == pnt.pside) {
					break;
				}

			}

			// Top-Left
			for (x1 = pnt.x-1, y1 = pnt.y-1; x1 >= MIN_X && y1 >= MIN_Y; x1--, y1--) {
				if (get_point_copy(grd, x1, y1).ptype == PTYPE_NONE) {
					pnt.valid_moves[x1][y1] = MTYPE_NORMAL;

				} else if (get_point_copy(grd, x1, y1).pside != pnt.pside) {
					pnt.valid_moves[x1][y1] = MTYPE_TAKE;
					break;

				} else if (get_point_copy(grd, x1, y1).pside == pnt.pside) {
					break;
				}
			}

			// Bot-Right
			for (x1 = pnt.x+1, y1 = pnt.y+1; x1 < MAX_X && y1 < MAX_Y; x1++, y1++) {
				if (get_point_copy(grd, x1, y1).ptype == PTYPE_NONE) {
					pnt.valid_moves[x1][y1] = MTYPE_NORMAL;

				} else if (get_point_copy(grd, x1, y1).pside != pnt.pside) {
					pnt.valid_moves[x1][y1] = MTYPE_TAKE;
					break;

				} else if (get_point_copy(grd, x1, y1).pside == pnt.pside) {
					break;
				}

			}

			// Bot-Left
			for (x1 = pnt.x-1, y1 = pnt.y+1; x1 >= MIN_X && y1 < MAX_Y; x1--, y1++) {
				if (get_point_copy(grd, x1, y1).ptype == PTYPE_NONE) {
					pnt.valid_moves[x1][y1] = MTYPE_NORMAL;

				} else if (get_point_copy(grd, x1, y1).pside != pnt.pside) {
					pnt.valid_moves[x1][y1] = MTYPE_TAKE;
					break;

				} else if (get_point_copy(grd, x1, y1).pside == pnt.pside) {
					break;
				}

			}

			break;

			;
			case PTYPE_KING:
				// Up

				if (get_point_copy(grd, pnt.x, pnt.y-1).ptype == PTYPE_NONE)
					pnt.valid_moves[pnt.x][pnt.y-1] = MTYPE_NORMAL;
					
				else if (get_point_copy(grd, pnt.x, pnt.y-1).pside != pnt.pside)
					pnt.valid_moves[pnt.x][pnt.y-1] = MTYPE_TAKE;

				// Down


				// Left

				// Right
	}
	grd->points[ploc_x][ploc_y] = pnt;

}

char get_ptype_char(POINT_TYPE ptype) {

	switch (ptype){
		case PTYPE_NONE:
		return '.';
		case PTYPE_PAWN:
		return 'P';
		case PTYPE_KING:
		return 'K';
		case PTYPE_QUEEN:
		return 'Q';
		case PTYPE_BISHOP:
		return 'B';
		case PTYPE_KNIGHT:
		return 'H';
		case PTYPE_ROOK:
		return 'R';
	}

	return '?';
}

void print_grid(Grid* grd, Point* pnt) {

	printf("\n");

	for (int y = MIN_Y; y < MAX_Y; y++) {
		printf("%d  ", MAX_Y-y);
		for (int x = MIN_X; x < MAX_X; x++) {
			
			switch (grd->points[x][y].pside) {
				case PSIDE_P1:
					printf("\033[0;33m");
					break;
				case PSIDE_P2:
					printf("\033[0;34m");
					break;
				case PSIDE_NONE:
					printf("\033[0;90m");
					break;
			}

			// If Point provided, highlight moveset of pnt
			if (pnt != NULL) {

				switch (pnt->valid_moves[x][y]){
					case MTYPE_NORMAL:
						printf("\033[0;102m");
						break;
					case MTYPE_TAKE:
						printf("\033[0;105m");
						break;
					case MTYPE_ENPASSANT:
						printf("\033[0;105m");
						break;
				}
				
				if (pnt->x == x && pnt->y == y){
					printf("\033[0;44m");
				}
			}

			printf("  %c  ", get_ptype_char(grd->points[x][y].ptype));

			// Clear ansi colors for next
			printf("\033[0;0m");
		}

		printf("\n\n");
	}

	// Return cursor up Grid
	printf("%s", "\n    A    B    C    D    E    F    G    H\n");
	//printf("\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A\x1b[A");
		
}

void start_chess() {

		Grid grd = {0};
		setup_grid(&grd);
		place_point(&grd, PTYPE_KING, PSIDE_P1, 3, 3);
		place_point(&grd, PTYPE_KING, PSIDE_P2, 3, 2);
		update_grid_context(&grd);
		print_grid(&grd, &grd.points[3][3]);
}
